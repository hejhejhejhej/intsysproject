package com.example.tiami.intsysproject.spoonacularJSONClasses;

import java.util.ArrayList;


public class WinePairing {
    private ArrayList<String> pairedWines;
    private String pairingText;
    private ArrayList<ProductMatch> productMatches;
    private String status;

    public WinePairing(ArrayList<String> pairedWines, String pairingText) {
        this.pairedWines = pairedWines;
        this.pairingText = pairingText;
        this.status = "succes";
    }

    public WinePairing(ArrayList<String> pairedWines, String pairingText, ArrayList<ProductMatch> productMatches, String status) {
        this.pairedWines = pairedWines;
        this.pairingText = pairingText;
        this.productMatches = productMatches;
        this.status = status;
    }

    public WinePairing(ArrayList<String> pairedWines, String pairingText, ArrayList<ProductMatch> productMatches) {
        this.pairedWines = pairedWines;
        this.pairingText = pairingText;
        this.productMatches = productMatches;
        this.status = "succes";
    }

    public ArrayList<String> getPairedWines() {
        return pairedWines;
    }

    public String getPairingText() {
        return pairingText;
    }

    public ArrayList<ProductMatch> getProductMatches() {
        return productMatches;
    }

    public String getStatus() {
        return status;
    }
}
