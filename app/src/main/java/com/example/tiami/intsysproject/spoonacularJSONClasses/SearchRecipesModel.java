package com.example.tiami.intsysproject.spoonacularJSONClasses;

import java.util.ArrayList;


public class SearchRecipesModel {
    private ArrayList<RecipeListItem> results;
    private String baseUri;
    private int offset;
    private int number;
    private int totalResults;
    private int processingTimeMs;
    private long expires;
    private boolean isStale;

    public SearchRecipesModel(ArrayList results, String baseUri, int offset, int number, int totalResults, int processingTimeMs, long expires, boolean isStale) {
        this.results = results;
        this.baseUri = baseUri;
        this.offset = offset;
        this.number = number;
        this.totalResults = totalResults;
        this.processingTimeMs = processingTimeMs;
        this.expires = expires;
        this.isStale = isStale;
    }

    public SearchRecipesModel(ArrayList<RecipeListItem> results, String baseUri) {
        this.results = results;
        this.baseUri = baseUri;
    }

    public ArrayList<RecipeListItem> getResults() {
        return results;
    }

    public String getBaseUri() {
        return baseUri;
    }

    public int getOffset() {
        return offset;
    }

    public int getNumber() {
        return number;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getProcessingTimeMs() {
        return processingTimeMs;
    }

    public long getExpires() {
        return expires;
    }

    public boolean isStale() {
        return isStale;
    }
}
