package com.example.tiami.intsysproject.spoonacularJSONClasses;


public class ProductMatch {
    private int id;
    private String title;
    private String description;
    private String price;
    private double averageRating;
    private int ratingCount;
    private double score;
    private String link;

    public ProductMatch(int id, String title, String description, String price, double averageRating, int ratingCount, double score, String link) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.price = price;
        this.averageRating = averageRating;
        this.ratingCount = ratingCount;
        this.score = score;
        this.link = link;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public double getAverageRating() {
        return averageRating;
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public double getScore() {
        return score;
    }

    public String getLink() {
        return link;
    }
}
