package com.example.tiami.intsysproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.tiami.intsysproject.constants.Constants;

/**
 * Created by tiami on 08/12/2017.
 */

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.d(Constants.TAG, "Created About activity");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Decides what menu items should do
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        CharSequence title = item.getTitle();
        changeActivity(title);
        return true;
    }

    //call changeActivity according to the selected option
    private void changeActivity(CharSequence title) {
        if(title.equals("Favourites")){
            Intent intent = new Intent(AboutActivity.this, FavouriteActivity.class);
            startActivity(intent);
        } else if(title.equals("FindWine")) {
            Intent intent = new Intent(AboutActivity.this, FindWineActivity.class);
            startActivity(intent);
        } else if(title.equals("About")) {
            Intent intent = new Intent(AboutActivity.this, AboutActivity.class);
            startActivity(intent);
        } else if(title.equals("Home")) {
            Intent intent = new Intent(AboutActivity.this, MainActivity.class);
            startActivity(intent);

        }
    }
}
