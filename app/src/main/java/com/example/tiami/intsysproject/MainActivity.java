package com.example.tiami.intsysproject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.tiami.intsysproject.constants.Constants;
import com.example.tiami.intsysproject.spoonacularJSONClasses.RandomRecipes;
import com.example.tiami.intsysproject.spoonacularJSONClasses.Recipe;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> ingredients;
    private ArrayList<String> excluded;
    private ArrayList<String> dietFilters;
    private ArrayList<String> allergyFilters;
    private int protein = 0;
    private int calory = 100000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        allergyFilters = new ArrayList<>();
        dietFilters = new ArrayList<>();

        Log.d(Constants.TAG, "Created main activity");

        ingredients = new ArrayList<>();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Decides what menu items should do
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        CharSequence title = item.getTitle();
        changeActivity(title);
        return true;
    }

    //call changeActivity according to the selected option
    private void changeActivity(CharSequence title) {
        if(title.equals("Favourites")){
            Intent intent = new Intent(MainActivity.this, FavouriteActivity.class);
            startActivity(intent);
        } else if(title.equals("FindWine")) {
            Intent intent = new Intent(MainActivity.this, FindWineActivity.class);
            startActivity(intent);
        } else if(title.equals("About")) {
            Intent intent = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(intent);
        } else if(title.equals("Home")) {
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent);

        }
    }


    public void addIngredient(View view) {
        final EditText ingredient = findViewById(R.id.addfield);
        ingredients.add(ingredient.getText().toString());

        ListView lw = findViewById(R.id.listIngredients);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                ingredients );
        lw.setAdapter(arrayAdapter);
        lw.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                ListView lw = findViewById(R.id.listIngredients);
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                        getBaseContext(),
                        android.R.layout.simple_list_item_1,
                        ingredients );

                    String item = arrayAdapter.getItem(position);
                    if(ingredients.contains(item)){
                        ingredients.remove(item);
                    }
                arrayAdapter = new ArrayAdapter<String>(
                        getBaseContext(),
                        android.R.layout.simple_list_item_1,
                        ingredients );
                lw.setAdapter(arrayAdapter);
                }
        });

        ingredient.setText("");
    }

    public void showFilter(View view) {
        Intent intent = new Intent(MainActivity.this, FilterActivity.class);
        startActivityForResult(intent,2);
    }

    public void showLucky(View view) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/random";

        // Request a string response
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //show response
                        System.out.println("Response: " + response);
                        Gson gson = new Gson();
                        RandomRecipes randomRecipes = gson.fromJson(response, RandomRecipes.class);
                        if (!randomRecipes.getRecipes().isEmpty()) {
                            Recipe recipe = randomRecipes.getRecipes().get(0);
                            response = gson.toJson(recipe, Recipe.class);
                            startRecipeActivity(response);
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("That didn't work!");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X-Mashape-Key", "n2gURhadr4msh3sohOaqzDXJ2E6ap1wd9fljsnoZ2WltuxnMmm");
                params.put("Accept", "application/json");
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==2)
        {
            String allergies = data.getStringExtra("ALLERGY");
            String diets = data.getStringExtra("DIET");

            if(allergies!=null) {
                String[] allerFilter = allergies.split(",");
                allergyFilters.addAll(Arrays.asList(allerFilter));
            }

            if(diets!=null) {
                String[] dietFilter = diets.split(",");
                dietFilters.addAll(Arrays.asList(dietFilter));
            }

            calory = data.getIntExtra("CALORY",0);
            protein = data.getIntExtra("PROTEIN",0);
            excluded = data.getStringArrayListExtra("EXCLUDED");
            System.out.println("calory = "+calory + " protein = " + protein);

            Context context = getApplicationContext();
            CharSequence text = "Filters added!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

        }
    }

    private void startRecipeActivity(String response) {
        Intent intent = new Intent(MainActivity.this, RecipeActivity.class);
        System.out.println("recipeAsJSON: " + response);
        intent.putExtra("recipe", response);
        startActivity(intent);
    }

    public void search(View view) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        //TODO: Get this from the result of FilterActivity
        StringBuilder sb = new StringBuilder();
        for(String s:ingredients){
            sb.append(s+"%2C+");
        }
        String includeIngredients = sb.toString();
        String excludeIngredients = "";
        sb = new StringBuilder();
        if(excluded!=null){
            sb.append("&excludeIngredients=");
            for(String s:excluded){
                sb.append(s+"%2C+");
            }
            excludeIngredients = sb.toString();
        }

        String allergyText = "";
        sb = new StringBuilder();
        if(allergyFilters!=null){
            sb.append("&intolerances=");
            for(String s:allergyFilters){
                sb.append(s+"%2C+");
            }
            allergyText = sb.toString();
        }

        String dietText = "";
        sb = new StringBuilder();
        if(dietFilters!=null){
            sb.append("&diet=");
            for(String s:dietFilters){
                sb.append(s+"%2C+");
            }
            dietText = sb.toString();
        }

        String url ="https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/searchComplex?addRecipeInformation=true" +
                "&includeIngredients=" + includeIngredients +
                excludeIngredients +
                "&instructionsRequired=false" +
                allergyText +
                dietText +
                "&limitLicense=false" +
                "&maxCalories=" + calory +
                "&minProtein=" + protein +
                "&number=10&offset=0" +
                "&query=" + includeIngredients +
                "&ranking=1";

        // Request a string response
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("Response: " + response);
                        Intent intent = new Intent(MainActivity.this,ResultActivity.class);
                        intent.putExtra("RECIPES",response);
                        startActivity(intent);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("That didn't work!");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X-Mashape-Key", "n2gURhadr4msh3sohOaqzDXJ2E6ap1wd9fljsnoZ2WltuxnMmm");
                params.put("Accept", "application/json");
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
