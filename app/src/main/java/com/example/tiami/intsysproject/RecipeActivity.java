package com.example.tiami.intsysproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.tiami.intsysproject.asyncTasks.DownloadImageTask;
import com.example.tiami.intsysproject.constants.Constants;
import com.example.tiami.intsysproject.spoonacularJSONClasses.Ingredient;
import com.example.tiami.intsysproject.spoonacularJSONClasses.Recipe;
import com.example.tiami.intsysproject.spoonacularJSONClasses.RecipeListItem;
import com.example.tiami.intsysproject.spoonacularJSONClasses.SearchRecipesModel;
import com.example.tiami.intsysproject.spoonacularJSONClasses.Step;
import com.example.tiami.intsysproject.spoonacularJSONClasses.WineDescription;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RecipeActivity extends AppCompatActivity {
    private Recipe recipe;
    private Gson gson;
    private String recipeString;
    private ImageView star;
    private boolean favourite;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recipe_activity);
        Intent intent = getIntent();
        recipeString = intent.getExtras().getString("recipe");
        System.out.println(recipeString);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gson = new Gson();

        recipe = gson.fromJson(recipeString, Recipe.class);
        System.out.println(recipe.getId());

        favourite = recipe.isFavourite();

        setFavouriteConfigurations();
        makeRecipe();
    }

    private void setFavouriteConfigurations() {
        star = findViewById(R.id.favourited);
        if (favourite) {
            star.setImageResource(R.drawable.star);
        }
        star.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SharedPreferences prefs = getSharedPreferences(
                        "com.example.app", Context.MODE_PRIVATE);
                String favouriteKey = "com.example.app.favourites";
                SharedPreferences.Editor editor = prefs.edit();

                Recipe favouriteRecipe = new Recipe(true,
                        recipe.getAggregateLikes(),
                        recipe.getExtendedIngredients(),
                        recipe.getId(),
                        recipe.getTitle(),
                        recipe.getReadyInMinutes(),
                        recipe.getImage(),
                        recipe.getDiets(),
                        recipe.getWinePairing(),
                        recipe.getAnalyzedInstructions());
                System.out.println("debubg her : " +recipe.getImage().substring(36));

                RecipeListItem favouriteListItem = new RecipeListItem(recipe.getId(),
                        recipe.getTitle(),
                        recipe.getReadyInMinutes(),
                        recipe.getImage(),
                        null,
                        favouriteRecipe);


                if (!favourite) {
                    star.setImageResource(R.drawable.star);
                    Toast.makeText(RecipeActivity.this, "Saved", Toast.LENGTH_SHORT).show();
                    favourite = true;

                    ArrayList<RecipeListItem> results = new ArrayList<>();


                    if (prefs.contains(favouriteKey)) {
                        String favouriteJSON = prefs.getString(favouriteKey,"");
                        SearchRecipesModel searchRecipesModel = gson.fromJson(favouriteJSON, SearchRecipesModel.class);
                        searchRecipesModel.getResults().add(favouriteListItem);
                        String toPreferences = gson.toJson(searchRecipesModel, SearchRecipesModel.class);
                        editor.putString(favouriteKey, toPreferences);
                        editor.commit();

                    } else {
                        results.add(favouriteListItem);
                        SearchRecipesModel searchRecipesModel = new SearchRecipesModel(results, Constants.BASE_URI);
                        String toPreferences = gson.toJson(searchRecipesModel, SearchRecipesModel.class);
                        editor.putString(favouriteKey, toPreferences);
                        editor.commit();
                    }

                } else {
                    star.setImageResource(R.drawable.empty_star);
                    favourite = false;
                    String favouriteJSON = prefs.getString(favouriteKey,"");
                    SearchRecipesModel searchRecipesModel = gson.fromJson(favouriteJSON, SearchRecipesModel.class);
                    System.out.println(searchRecipesModel.getResults().get(0).getId());
                    int itemIndex = 0;
                    for (RecipeListItem item: searchRecipesModel.getResults()) {

                        if (item.getId() == favouriteListItem.getId()) {
                            System.out.println(item.getId());
                            searchRecipesModel.getResults().remove(itemIndex);
                            break;
                        }
                        itemIndex++;
                    }
                    String toPreferences = gson.toJson(searchRecipesModel, SearchRecipesModel.class);
                    editor.putString(favouriteKey, toPreferences);
                    editor.commit();
                }
            }
        });
    }

    private void populateWineRecommendations() {
        if (!(recipe.getWinePairing() == null)) {
            ArrayList<String> wines = recipe.getWinePairing().getPairedWines();
            if (wines != null) {
                LinearLayout wineLayout = findViewById(R.id.wineRecommendations);
                TextView textView = new TextView(this);
                textView.setText(R.string.wineReq);
                textView.setTextSize(20);
                textView.setPadding(0,0,0,5);
                textView.setTypeface(null, Typeface.BOLD);
                wineLayout.addView(textView);
                for (String wine: wines) {
                    TextView WineTextView = new TextView(this);
                    String s1 = String.valueOf(wine.charAt(0)).toUpperCase();
                    String s2 = wine.substring(1);
                    WineTextView.setText(s1+s2+ ": ");
                    WineTextView.setTypeface(null, Typeface.BOLD);
                    wineLayout.addView(WineTextView);

                    TextView wineDescription = new TextView(this);
                    wineLayout.addView(wineDescription);
                    sendRequest(s1+s2, wineDescription);
                }

                TextView pairingTextView = new TextView(this);
                pairingTextView.setText(recipe.getWinePairing().getPairingText());
                wineLayout.addView(pairingTextView);
            }
        }
    }

    private void populateIngredientsView() {
        LinearLayout ingredientListView = findViewById(R.id.recipeIngredients);
        TextView ingredientText = new TextView(this);
        ingredientText.setText(R.string.ingrtext);
        ingredientText.setTextSize(25);
        ingredientListView.addView(ingredientText);

        for (Ingredient ingredient: recipe.getExtendedIngredients()) {
            ingredientText = new TextView(this);
            ingredientText.setText(ingredient.getAmount() + " " + ingredient.getUnit() + " " + ingredient.getName());
            ingredientListView.addView(ingredientText);
        }
    }

    private void makeRecipe() {
        populateImageAndIcons();
        populateAllergiesAndDiets();
        populateInstructions();
        populateIngredientsView();
        populateWineRecommendations();
    }

    private void populateAllergiesAndDiets() {
        LinearLayout diets = findViewById(R.id.diets);
        TextView header = new TextView(this);
        header.setText(R.string.recipeText);
        header.setTypeface(null, Typeface.BOLD);
        diets.addView(header);
        for (String diet: recipe.getDiets()) {
            TextView dietTextView = new TextView(this);
            String s1 = String.valueOf(diet.charAt(0)).toUpperCase();
            String s2 = diet.substring(1);
            dietTextView.setText(s1 + s2);
            diets.addView(dietTextView);
        }
    }

    private void populateInstructions() {
        LinearLayout instructions = findViewById(R.id.instructions);

        System.out.println(recipe.getAnalyzedInstructions());
        if (!recipe.getAnalyzedInstructions().isEmpty()) {
            for (Step step : recipe.getAnalyzedInstructions().get(0).getSteps()) {
                TextView stepNumber = new TextView(this);
                TextView instructionText = new TextView(this);

                stepNumber.setText("Step " + step.getNumber()+ ": ");
                stepNumber.setTypeface(null, Typeface.BOLD);
                instructionText.setText(step.getStep());

                instructions.addView(stepNumber);
                instructions.addView(instructionText);
            }

        } else {
            TextView instructionText = new TextView(this);
            instructionText.setText("No instructions for this recipe");
            instructions.addView(instructionText);
        }

    }

    private void populateImageAndIcons() {
        ImageView mainImage = findViewById(R.id.mainImage);
        TextView likes = findViewById(R.id.amountOfLikes);
        TextView minutes = findViewById(R.id.amountOfMinutes);
        TextView title = findViewById(R.id.recipeTitle);


        new DownloadImageTask(mainImage).execute(recipe.getImage());
        System.out.println(recipe.getAggregateLikes());
        likes.setText(String.valueOf(recipe.getAggregateLikes()) + " likes");
        minutes.setText(String.valueOf(recipe.getReadyInMinutes()) + " minutes");
        title.setText(recipe.getTitle());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Decides what menu items should do
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        CharSequence title = item.getTitle();
        changeActivity(title);
        return true;
    }

    //call changeActivity according to the selected option
    private void changeActivity(CharSequence title) {
        if(title.equals("Favourites")){
            Intent intent = new Intent(RecipeActivity.this, FavouriteActivity.class);
            startActivity(intent);
        } else if(title.equals("FindWine")) {
            Intent intent = new Intent(RecipeActivity.this, FindWineActivity.class);
            startActivity(intent);
        } else if(title.equals("About")) {
            Intent intent = new Intent(RecipeActivity.this, AboutActivity.class);
            startActivity(intent);
        } else if(title.equals("Home")) {
            Intent intent = new Intent(RecipeActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

    private void sendRequest(String wine, final TextView view) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/food/wine/description?wine="+wine;

        // Request a string response
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //save response
                        System.out.println("Response: " + response);
                        WineDescription wineDescription = gson.fromJson(response, WineDescription.class);
                        view.setText(wineDescription.getWineDescription());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("That didn't work!");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X-Mashape-Key", "n2gURhadr4msh3sohOaqzDXJ2E6ap1wd9fljsnoZ2WltuxnMmm");
                params.put("Accept", "application/json");
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }



}
