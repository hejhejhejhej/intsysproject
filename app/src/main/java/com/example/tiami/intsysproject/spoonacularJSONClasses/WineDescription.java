package com.example.tiami.intsysproject.spoonacularJSONClasses;

public class WineDescription {
    private String wineDescription;

    public WineDescription(String wineDescription) {
        this.wineDescription = wineDescription;
    }

    public String getWineDescription() {
        return wineDescription;
    }

    public void setWineDescription(String wineDescription) {
        this.wineDescription = wineDescription;
    }
}
