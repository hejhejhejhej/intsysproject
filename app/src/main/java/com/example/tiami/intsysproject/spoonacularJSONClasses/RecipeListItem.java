package com.example.tiami.intsysproject.spoonacularJSONClasses;

import java.util.ArrayList;


public class RecipeListItem {
    private int id;
    private String title;
    private int readyInMinutes;
    private String image;
    private ArrayList<String> imageUrls;
    private Recipe recipe;

    public RecipeListItem(int id, String title, int readyInMinutes, String image, ArrayList<String> imageUrls, Recipe recipe) {
        this.id = id;
        this.title = title;
        this.readyInMinutes = readyInMinutes;
        this.image = image;
        this.imageUrls = imageUrls;
        this.recipe = recipe;
    }

    public RecipeListItem(int id, String title, int readyInMinutes, String image, ArrayList<String> imageUrls) {
        this.id = id;
        this.title = title;
        this.readyInMinutes = readyInMinutes;
        this.image = image;
        this.imageUrls = imageUrls;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getReadyInMinutes() {
        return readyInMinutes;
    }

    public String getImage() {
        return image;
    }

    public ArrayList<String> getImageUrls() {
        return imageUrls;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setReadyInMinutes(int readyInMinutes) {
        this.readyInMinutes = readyInMinutes;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setImageUrls(ArrayList<String> imageUrls) {
        this.imageUrls = imageUrls;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == this) {
            return true;
        }

        if (!(obj instanceof RecipeListItem)) {
            return false;
        }

        RecipeListItem item = (RecipeListItem) obj;

        if ( item.getId() == id) {
            return true;
        }
        return false;
    }
}
