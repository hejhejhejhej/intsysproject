package com.example.tiami.intsysproject;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.tiami.intsysproject.spoonacularJSONClasses.WinePairing;
import com.example.tiami.intsysproject.spoonacularJSONClasses.*;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Soren on 05/12/2017.
 */

public class FindWineActivity extends AppCompatActivity {
    Button searchButton;
    String winePairingAsJSON;
    WinePairing winePairing;
    Gson gson;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.findwine_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gson = new Gson();

        searchButton = findViewById(R.id.wineSearchButton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText wineEditTextField = findViewById(R.id.wineEditTextField);
                sendRequestForWinePairing(wineEditTextField.getText().toString());
            }
        });
    }

    private void method() {
        if (!(winePairing == null)) {
            ArrayList<String> wines = winePairing.getPairedWines();
            LinearLayout wineLayout = findViewById(R.id.wineRecommendationsFindWine);

            if (!wines.isEmpty()) {
                TextView textView = new TextView(this);
                textView.setText("Wine recommendations: ");
                textView.setTextSize(20);
                textView.setPadding(0,0,0,5);
                textView.setTypeface(null, Typeface.BOLD);
                wineLayout.addView(textView);
                for (String wine: wines) {
                    TextView WineTextView = new TextView(this);
                    String s1 = String.valueOf(wine.charAt(0)).toUpperCase();
                    String s2 = wine.substring(1);
                    String s3 = s1+s2+ ": ";
                    WineTextView.setText(s3);
                    WineTextView.setTypeface(null, Typeface.BOLD);
                    wineLayout.addView(WineTextView);

                    TextView wineDescription = new TextView(this);
                    wineLayout.addView(wineDescription);
                    sendRequestForWineDescription(s1+s2, wineDescription);
                }

                TextView pairingTextView = new TextView(this);
                pairingTextView.setText(winePairing.getPairingText());
                wineLayout.addView(pairingTextView);
            }
            TextView textView;
            ArrayList<ProductMatch> productMatches = winePairing.getProductMatches();

            if (productMatches!=null) {
                for (ProductMatch product: productMatches) {
                    textView = new TextView(this);
                    textView.setText(product.getTitle());
                    textView.setTextSize(20);
                    textView.setTypeface(null, Typeface.BOLD);
                    wineLayout.addView(textView);
                    textView = new TextView(this);
                    textView.setText(product.getDescription());
                    wineLayout.addView(textView);
                    textView = new TextView(this);
                    String price = "Price: " + product.getPrice();
                    textView.setText(price);
                    wineLayout.addView(textView);
                    textView = new TextView(this);
                    String text = "<a href='"+ product.getLink()+"'> Link </a>";
                    textView.setText(Html.fromHtml(text));
                    textView.setClickable(true);
                    textView.setMovementMethod(LinkMovementMethod.getInstance());
                    wineLayout.addView(textView);
                }
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Decides what menu items should do
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        CharSequence title = item.getTitle();
        changeActivity(title);
        return true;
    }

    //call changeActivity according to the selected option
    private void changeActivity(CharSequence title) {
        if(title.equals("Favourites")){
            Intent intent = new Intent(FindWineActivity.this, FavouriteActivity.class);
            startActivity(intent);
        } else if(title.equals("FindWine")) {
            Intent intent = new Intent(FindWineActivity.this, FindWineActivity.class);
            startActivity(intent);
        } else if(title.equals("About")) {
            Intent intent = new Intent(FindWineActivity.this, AboutActivity.class);
            startActivity(intent);
        } else if(title.equals("Home")) {
            Intent intent = new Intent(FindWineActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }
    private void sendRequestForWineDescription(String wine, final TextView view) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/food/wine/description?wine="+wine;

        // Request a string response
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //save response
                        WineDescription wineDescription = gson.fromJson(response, WineDescription.class);
                        view.setText(wineDescription.getWineDescription());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("That didn't work!");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X-Mashape-Key", "n2gURhadr4msh3sohOaqzDXJ2E6ap1wd9fljsnoZ2WltuxnMmm");
                params.put("Accept", "application/json");
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void sendRequestForWinePairing(final String food) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/food/wine/pairing?food=" + food + "&maxPrice=50";

        // Request a string response
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //save response
                        System.out.println("Response: " + response);
                        winePairingAsJSON = response;
                        winePairing = gson.fromJson(winePairingAsJSON, WinePairing.class);
                        if (winePairingAsJSON.contains("failure")) {
                            Toast.makeText(FindWineActivity.this, ("Could not find wine for " + food), Toast.LENGTH_SHORT).show();
                        } else {
                            method();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("That didn't work!");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X-Mashape-Key", "n2gURhadr4msh3sohOaqzDXJ2E6ap1wd9fljsnoZ2WltuxnMmm");
                params.put("Accept", "application/json");
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }
}
