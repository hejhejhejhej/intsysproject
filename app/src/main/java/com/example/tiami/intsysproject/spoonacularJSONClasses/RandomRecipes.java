package com.example.tiami.intsysproject.spoonacularJSONClasses;

import java.util.ArrayList;


public class RandomRecipes {
    private ArrayList<Recipe> recipes;

    public RandomRecipes(ArrayList<Recipe> recipes) {
        this.recipes = recipes;
    }

    public ArrayList<Recipe> getRecipes() {
        return recipes;
    }
}
