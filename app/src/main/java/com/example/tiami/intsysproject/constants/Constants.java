package com.example.tiami.intsysproject.constants;

public class Constants {

    // BuildMyString.com generated code. Please enjoy your string responsibly.
    public static String TAG = "DinnerTime";
    public static String BASE_URI = "\"https://spoonacular.com/recipeImages/\"";
    public static String RESULT_TEST_STRING = "{" +
            "  \"results\": [" +
            "    {" +
            "      \"id\": 262682," +
            "      \"title\": \"Thai Sweet Potato Veggie Burgers with Spicy Peanut Sauce\"," +
            "      \"readyInMinutes\": 75," +
            "      \"image\": \"thai-sweet-potato-veggie-burgers-with-spicy-peanut-sauce-262682.jpg\"," +
            "      \"imageUrls\": [" +
            "        \"thai-sweet-potato-veggie-burgers-with-spicy-peanut-sauce-262682.jpg\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 227961," +
            "      \"title\": \"Cajun Spiced Black Bean and Sweet Potato Burgers\"," +
            "      \"readyInMinutes\": 20," +
            "      \"image\": \"Cajun-Spiced-Black-Bean-and-Sweet-Potato-Burgers-227961.jpg\"," +
            "      \"imageUrls\": [" +
            "        \"Cajun-Spiced-Black-Bean-and-Sweet-Potato-Burgers-227961.jpg\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 602708," +
            "      \"title\": \"Meatless Monday: Grilled Portobello Mushroom Burgers with Romesco and Arugula\"," +
            "      \"readyInMinutes\": 15," +
            "      \"image\": \"Meatless-Monday--Grilled-Portobello-Mushroom-Burgers-with-Romesco-and-Arugula-602708.jpg\"," +
            "      \"imageUrls\": [" +
            "        \"Meatless-Monday--Grilled-Portobello-Mushroom-Burgers-with-Romesco-and-Arugula-602708.jpg\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 759739," +
            "      \"title\": \"Gluten-Free Veggie Burger\"," +
            "      \"readyInMinutes\": 45," +
            "      \"image\": \"gluten-free-veggie-burger-759739.jpg\"," +
            "      \"imageUrls\": [" +
            "        \"gluten-free-veggie-burger-759739.jpg\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 630255," +
            "      \"title\": \"Protein Powerhouse Veggie Burgers\"," +
            "      \"readyInMinutes\": 95," +
            "      \"image\": \"Protein-Powerhouse-Veggie-Burgers-630255.jpg\"," +
            "      \"imageUrls\": [" +
            "        \"Protein-Powerhouse-Veggie-Burgers-630255.jpg\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 479732," +
            "      \"title\": \"Meatless Monday: Curried Veggie Burgers with Zucchini, Lentils, and Quinoa\"," +
            "      \"readyInMinutes\": 15," +
            "      \"image\": \"Meatless-Monday--Curried-Veggie-Burgers-with-Zucchini--Lentils--and-Quinoa-479732.jpg\"," +
            "      \"imageUrls\": [" +
            "        \"Meatless-Monday--Curried-Veggie-Burgers-with-Zucchini--Lentils--and-Quinoa-479732.jpg\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 541691," +
            "      \"title\": \"Black Bean Mole Burgers\"," +
            "      \"readyInMinutes\": 45," +
            "      \"image\": \"black-bean-mole-burgers-541691.jpg\"," +
            "      \"imageUrls\": [" +
            "        \"black-bean-mole-burgers-541691.jpg\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 34035," +
            "      \"title\": \"Sprouted Lentil Veggie Burger\"," +
            "      \"readyInMinutes\": 30," +
            "      \"image\": \"sprouted_lentil_veggie_burger-34035.jpg\"," +
            "      \"imageUrls\": [" +
            "        \"sprouted_lentil_veggie_burger-34035.jpg\"," +
            "        \"sprouted-lentil-veggie-burger-2-34035.jpg\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 766301," +
            "      \"title\": \"Queso Cheese Burgers\"," +
            "      \"readyInMinutes\": 60," +
            "      \"image\": \"queso-cheese-burgers-766301.jpg\"," +
            "      \"imageUrls\": [" +
            "        \"queso-cheese-burgers-766301.jpg\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 761774," +
            "      \"title\": \"Simple Soybean Burgers\"," +
            "      \"readyInMinutes\": 45," +
            "      \"image\": \"simple-soybean-burgers-761774.jpg\"," +
            "      \"imageUrls\": [" +
            "        \"simple-soybean-burgers-761774.jpg\"" +
            "      ]" +
            "    }" +
            "  ]," +
            "  \"baseUri\": \"https://spoonacular.com/recipeImages/\"," +
            "  \"offset\": 0," +
            "  \"number\": 10," +
            "  \"totalResults\": 10," +
            "  \"processingTimeMs\": 323," +
            "  \"expires\": 1473587241426," +
            "  \"isStale\": false" +
            "}";
    public static String RECIPE_TEST_STRING_1 = "{" +
            "  \"vegetarian\": true," +
            "  \"vegan\": false," +
            "  \"glutenFree\": true," +
            "  \"dairyFree\": false," +
            "  \"veryHealthy\": false," +
            "  \"cheap\": false," +
            "  \"veryPopular\": false," +
            "  \"sustainable\": false," +
            "  \"weightWatcherSmartPoints\": 38," +
            "  \"gaps\": \"no\"," +
            "  \"lowFodmap\": false," +
            "  \"ketogenic\": true," +
            "  \"whole30\": false," +
            "  \"servings\": 4," +
            "  \"sourceUrl\": \"http://www.howsweeteats.com/2016/05/queso-cheese-burgers-with-avocado-and-pico-de-gallo/\"," +
            "  \"spoonacularSourceUrl\": \"https://spoonacular.com/queso-cheese-burgers-766301\"," +
            "  \"aggregateLikes\": 149," +
            "  \"spoonacularScore\": 84," +
            "  \"healthScore\": 23," +
            "  \"creditText\": \"How Sweet Eats\"," +
            "  \"sourceName\": \"How Sweet Eats\"," +
            "  \"pricePerServing\": 306.32," +
            "  \"extendedIngredients\": [" +
            "    {" +
            "      \"id\": 9037," +
            "      \"aisle\": \"Produce\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/avocado.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"avocados\"," +
            "      \"amount\": 2," +
            "      \"unit\": \"\"," +
            "      \"unitShort\": \"\"," +
            "      \"unitLong\": \"\"," +
            "      \"originalString\": \"2 avocados, thinly sliced\"," +
            "      \"metaInformation\": [" +
            "        \"thinly sliced\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 20027," +
            "      \"aisle\": \"Baking\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/white-powder.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"cornstarch\"," +
            "      \"amount\": 2," +
            "      \"unit\": \"tablespoons\"," +
            "      \"unitShort\": \"Tbsp\"," +
            "      \"unitLong\": \"tablespoons\"," +
            "      \"originalString\": \"2 tablespoons cornstarch\"," +
            "      \"metaInformation\": []" +
            "    }," +
            "    {" +
            "      \"id\": 11215," +
            "      \"aisle\": \"Produce\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/garlic.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"garlic cloves\"," +
            "      \"amount\": 2," +
            "      \"unit\": \"\"," +
            "      \"unitShort\": \"\"," +
            "      \"unitLong\": \"\"," +
            "      \"originalString\": \"2 garlic cloves, minced\"," +
            "      \"metaInformation\": [" +
            "        \"minced\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 1049," +
            "      \"aisle\": \"Milk, Eggs, Other Dairy\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/fluid-cream.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"half and half\"," +
            "      \"amount\": 1," +
            "      \"unit\": \"cups\"," +
            "      \"unitShort\": \"cup\"," +
            "      \"unitLong\": \"cup\"," +
            "      \"originalString\": \"1 cups half and half\"," +
            "      \"metaInformation\": []" +
            "    }," +
            "    {" +
            "      \"id\": 9160," +
            "      \"aisle\": \"Produce\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/lime-juice.jpg\"," +
            "      \"consistency\": \"liquid\"," +
            "      \"name\": \"juice of lime\"," +
            "      \"amount\": 1," +
            "      \"unit\": \"\"," +
            "      \"unitShort\": \"\"," +
            "      \"unitLong\": \"\"," +
            "      \"originalString\": \"1 lime, juiced\"," +
            "      \"metaInformation\": [" +
            "        \"juiced\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 1001025," +
            "      \"aisle\": \"Cheese\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/shredded-cheese-white.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"monterey jack cheese\"," +
            "      \"amount\": 8," +
            "      \"unit\": \"ounces\"," +
            "      \"unitShort\": \"oz\"," +
            "      \"unitLong\": \"ounces\"," +
            "      \"originalString\": \"8 ounces monterey jack cheese, freshly grated\"," +
            "      \"metaInformation\": [" +
            "        \"freshly grated\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 4053," +
            "      \"aisle\": \"Oil, Vinegar, Salad Dressing\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/olive-oil.jpg\"," +
            "      \"consistency\": \"liquid\"," +
            "      \"name\": \"olive oil\"," +
            "      \"amount\": 1," +
            "      \"unit\": \"tablespoon\"," +
            "      \"unitShort\": \"Tbsp\"," +
            "      \"unitLong\": \"tablespoon\"," +
            "      \"originalString\": \"1 tablespoon olive oil\"," +
            "      \"metaInformation\": []" +
            "    }," +
            "    {" +
            "      \"id\": 1002030," +
            "      \"aisle\": \"Spices and Seasonings\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/pepper.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"pepper\"," +
            "      \"amount\": 0.125," +
            "      \"unit\": \"teaspoon\"," +
            "      \"unitShort\": \"tsp\"," +
            "      \"unitLong\": \"teaspoons\"," +
            "      \"originalString\": \"1/8 teaspoon pepper\"," +
            "      \"metaInformation\": []" +
            "    }," +
            "    {" +
            "      \"id\": 2047," +
            "      \"aisle\": \"Spices and Seasonings\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/salt.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"salt\"," +
            "      \"amount\": 0.125," +
            "      \"unit\": \"teaspoon\"," +
            "      \"unitShort\": \"tsp\"," +
            "      \"unitLong\": \"teaspoons\"," +
            "      \"originalString\": \"1/8 teaspoon salt\"," +
            "      \"metaInformation\": []" +
            "    }," +
            "    {" +
            "      \"id\": 1031009," +
            "      \"aisle\": \"Cheese\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/cheddar-cheese.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"sharp cheddar cheese\"," +
            "      \"amount\": 16," +
            "      \"unit\": \"ounces\"," +
            "      \"unitShort\": \"oz\"," +
            "      \"unitLong\": \"ounces\"," +
            "      \"originalString\": \"16 ounces sharp white cheddar cheese, freshly grated\"," +
            "      \"metaInformation\": [" +
            "        \"white\"," +
            "        \"freshly grated\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 11294," +
            "      \"aisle\": \"Produce\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/sweet-onion.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"sweet onion\"," +
            "      \"amount\": 0.5," +
            "      \"unit\": \"\"," +
            "      \"unitShort\": \"\"," +
            "      \"unitLong\": \"\"," +
            "      \"originalString\": \"1/2 sweet onion, diced\"," +
            "      \"metaInformation\": [" +
            "        \"diced\"," +
            "        \"sweet\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 1145," +
            "      \"aisle\": \"Milk, Eggs, Other Dairy\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/butter-sliced.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"unsalted butter\"," +
            "      \"amount\": 1," +
            "      \"unit\": \"tablespoon\"," +
            "      \"unitShort\": \"Tbsp\"," +
            "      \"unitLong\": \"tablespoon\"," +
            "      \"originalString\": \"1 tablespoon unsalted butter\"," +
            "      \"metaInformation\": [" +
            "        \"unsalted\"" +
            "      ]" +
            "    }" +
            "  ]," +
            "  \"id\": 766301," +
            "  \"title\": \"Queso Cheese Burgers\"," +
            "  \"readyInMinutes\": 60," +
            "  \"image\": \"https://spoonacular.com/recipeImages/766301-556x370.jpg\"," +
            "  \"imageType\": \"jpg\"," +
            "  \"cuisines\": [" +
            "    \"american\"" +
            "  ]," +
            "  \"dishTypes\": [" +
            "    \"lunch\"," +
            "    \"main course\"," +
            "    \"main dish\"," +
            "    \"dinner\"" +
            "  ]," +
            "  \"diets\": [" +
            "    \"gluten free\"," +
            "    \"lacto ovo vegetarian\"," +
            "    \"ketogenic\"" +
            "  ]," +
            "  \"occasions\": []," +
            "  \"winePairing\": {" +
            "    \"pairedWines\": [" +
            "      \"malbec\"," +
            "      \"merlot\"," +
            "      \"zinfandel\"" +
            "    ]," +
            "    \"pairingText\": \"Cheeseburger works really well with Malbec, Merlot, and Zinfandel. Merlot will be perfectly adequate for a classic burger with standard toppings. Bolder toppings call for bolder wines, such as a malbec or peppery zinfandel. The Luigi Bosca Malbec with a 4.2 out of 5 star rating seems like a good match. It costs about 23 dollars per bottle.\"," +
            "    \"productMatches\": [" +
            "      {" +
            "        \"id\": 438334," +
            "        \"title\": \"Luigi Bosca Malbec\"," +
            "        \"description\": \"Luigi Bosca Malbec is an intense purple colour wine, with distinct aromas of ripe red fruits, spices and black pepper. The intensity of the entry in the mouth is complemented with the softness and sweetness of tannins, respecting the characteristics of the grape variety in Argentina. A pure, full-bodied, well-structured red wine with character and all the juiciness typical of this variety. A long lingering and elegant finish.\"," +
            "        \"price\": \"$22.99\"," +
            "        \"imageUrl\": \"https://spoonacular.com/productImages/438334-312x231.jpg\"," +
            "        \"averageRating\": 0.8400000000000001," +
            "        \"ratingCount\": 13," +
            "        \"score\": 0.8150000000000001," +
            "        \"link\": \"https://click.linksynergy.com/deeplink?id=*QCiIS6t4gA&mid=2025&murl=https%3A%2F%2Fwww.wine.com%2Fproduct%2Fluigi-bosca-malbec-2011%2F132493\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  \"instructions\": \"burgersPlace the beef in a bowl and season with the salt and pepper. Mix with your hands to incorporate, forming 4 to 6 patties, whichever size you wish and whatever size fits your bun. Heat a large skillet (or thegrill, your preference) over medium heat and add the olive oil and butter. Cook the patties until browned on both sides and your desired doneness is reached. I do about 3 to 4 minutes per side for medium-well, but it will depend on how think your burgers are.In a bowl, mix together the tomatoes, onion, cilantro and lime juiced, stirring well. Season with a pinch of salt and pepper.To assemble your burgers, cover the bottom bun with avocado slices. Place the burger on top and drizzle on lots of queso cheese. Top with the pico and the jalapeos. Serve immediately.queso cheeseHeat a saucepan over medium heat and add the olive oil and butter. Stir in the onions and garlic. Season with the salt and pepper. Cook until the onion softens, about 5 to 6 minutes. Slowly stream in 3/4 cup of the half and half, whisking the entire time. Toss the grated cheese with 1 tablespoon of the cornstarch. In a bowl, whisk together the remaining 1/4 cup half and half and 1 tablespoon cornstarch until no lumps remain to create a slurry. Stir the slurry into the saucepan and cook for a minute until the milk thickens. Reduce the heat to low.Stir in the grated cheese, one small handful at a time, until melted. Transfer the mixture to a crock, larger bowl or a small crockpot heating on low. Spoon on the burgers and serve the extra with chips!\"," +
            "  \"analyzedInstructions\": [" +
            "    {" +
            "      \"name\": \"\"," +
            "      \"steps\": [" +
            "        {" +
            "          \"number\": 1," +
            "          \"step\": \"burgers\"," +
            "          \"ingredients\": []," +
            "          \"equipment\": []" +
            "        }," +
            "        {" +
            "          \"number\": 2," +
            "          \"step\": \"Place the beef in a bowl and season with the salt and pepper.\"," +
            "          \"ingredients\": [" +
            "            {" +
            "              \"id\": 1102047," +
            "              \"name\": \"salt and pepper\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/salt-and-pepper.jpg\"" +
            "            }" +
            "          ]," +
            "          \"equipment\": [" +
            "            {" +
            "              \"id\": 404783," +
            "              \"name\": \"bowl\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/bowl.jpg\"" +
            "            }" +
            "          ]" +
            "        }," +
            "        {" +
            "          \"number\": 3," +
            "          \"step\": \"Mix with your hands to incorporate, forming 4 to 6 patties, whichever size you wish and whatever size fits your bun.\"," +
            "          \"ingredients\": []," +
            "          \"equipment\": []" +
            "        }," +
            "        {" +
            "          \"number\": 4," +
            "          \"step\": \"Heat a large skillet (or thegrill, your preference) over medium heat and add the olive oil and butter. Cook the patties until browned on both sides and your desired doneness is reached. I do about 3 to 4 minutes per side for medium-well, but it will depend on how think your burgers are.In a bowl, mix together the tomatoes, onion, cilantro and lime juiced, stirring well. Season with a pinch of salt and pepper.To assemble your burgers, cover the bottom bun with avocado slices.\"," +
            "          \"ingredients\": [" +
            "            {" +
            "              \"id\": 1102047," +
            "              \"name\": \"salt and pepper\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/salt-and-pepper.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 1019037," +
            "              \"name\": \"avocado slices\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/avocado-slices.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 4053," +
            "              \"name\": \"olive oil\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/olive-oil.jpg\"" +
            "            }" +
            "          ]," +
            "          \"equipment\": [" +
            "            {" +
            "              \"id\": 404645," +
            "              \"name\": \"frying pan\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/pan.png\"" +
            "            }," +
            "            {" +
            "              \"id\": 404783," +
            "              \"name\": \"bowl\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/bowl.jpg\"" +
            "            }" +
            "          ]," +
            "          \"length\": {" +
            "            \"number\": 3," +
            "            \"unit\": \"minutes\"" +
            "          }" +
            "        }," +
            "        {" +
            "          \"number\": 5," +
            "          \"step\": \"Place the burger on top and drizzle on lots of queso cheese. Top with the pico and the jalapeos.\"," +
            "          \"ingredients\": [" +
            "            {" +
            "              \"id\": 1041009," +
            "              \"name\": \"cheese\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/cheddar-cheese.jpg\"" +
            "            }" +
            "          ]," +
            "          \"equipment\": []" +
            "        }," +
            "        {" +
            "          \"number\": 6," +
            "          \"step\": \"Serve immediately.queso cheese\"," +
            "          \"ingredients\": [" +
            "            {" +
            "              \"id\": 1041009," +
            "              \"name\": \"cheese\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/cheddar-cheese.jpg\"" +
            "            }" +
            "          ]," +
            "          \"equipment\": []" +
            "        }," +
            "        {" +
            "          \"number\": 7," +
            "          \"step\": \"Heat a saucepan over medium heat and add the olive oil and butter. Stir in the onions and garlic. Season with the salt and pepper. Cook until the onion softens, about 5 to 6 minutes. Slowly stream in 3/4 cup of the half and half, whisking the entire time. Toss the grated cheese with 1 tablespoon of the cornstarch. In a bowl, whisk together the remaining 1/4 cup half and half and 1 tablespoon cornstarch until no lumps remain to create a slurry. Stir the slurry into the saucepan and cook for a minute until the milk thickens. Reduce the heat to low.Stir in the grated cheese, one small handful at a time, until melted.\"," +
            "          \"ingredients\": [" +
            "            {" +
            "              \"id\": 1102047," +
            "              \"name\": \"salt and pepper\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/salt-and-pepper.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 1049," +
            "              \"name\": \"half and half\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/fluid-cream.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 20027," +
            "              \"name\": \"corn starch\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/white-powder.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 4053," +
            "              \"name\": \"olive oil\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/olive-oil.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 1041009," +
            "              \"name\": \"cheese\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/cheddar-cheese.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 11215," +
            "              \"name\": \"garlic\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/garlic.jpg\"" +
            "            }" +
            "          ]," +
            "          \"equipment\": [" +
            "            {" +
            "              \"id\": 404669," +
            "              \"name\": \"sauce pan\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/sauce-pan.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 404661," +
            "              \"name\": \"whisk\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/whisk.png\"" +
            "            }," +
            "            {" +
            "              \"id\": 404783," +
            "              \"name\": \"bowl\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/bowl.jpg\"" +
            "            }" +
            "          ]," +
            "          \"length\": {" +
            "            \"number\": 5," +
            "            \"unit\": \"minutes\"" +
            "          }" +
            "        }," +
            "        {" +
            "          \"number\": 8," +
            "          \"step\": \"Transfer the mixture to a crock, larger bowl or a small crockpot heating on low. Spoon on the burgers and serve the extra with chips!\"," +
            "          \"ingredients\": []," +
            "          \"equipment\": [" +
            "            {" +
            "              \"id\": 404718," +
            "              \"name\": \"slow cooker\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/slow-cooker.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 404783," +
            "              \"name\": \"bowl\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/bowl.jpg\"" +
            "            }" +
            "          ]" +
            "        }" +
            "      ]" +
            "    }" +
            "  ]" +
            "}";
    public static String RECIPE_TEST_STRING_2 = "{" +
            "  \"vegetarian\": true," +
            "  \"vegan\": false," +
            "  \"glutenFree\": true," +
            "  \"dairyFree\": false," +
            "  \"veryHealthy\": false," +
            "  \"cheap\": false," +
            "  \"veryPopular\": false," +
            "  \"sustainable\": false," +
            "  \"weightWatcherSmartPoints\": 38," +
            "  \"gaps\": \"no\"," +
            "  \"lowFodmap\": false," +
            "  \"ketogenic\": true," +
            "  \"whole30\": false," +
            "  \"servings\": 4," +
            "  \"sourceUrl\": \"http://www.howsweeteats.com/2016/05/queso-cheese-burgers-with-avocado-and-pico-de-gallo/\"," +
            "  \"spoonacularSourceUrl\": \"https://spoonacular.com/queso-cheese-burgers-766301\"," +
            "  \"aggregateLikes\": 149," +
            "  \"spoonacularScore\": 84," +
            "  \"healthScore\": 23," +
            "  \"creditText\": \"How Sweet Eats\"," +
            "  \"sourceName\": \"How Sweet Eats\"," +
            "  \"pricePerServing\": 306.32," +
            "  \"extendedIngredients\": [" +
            "    {" +
            "      \"id\": 9037," +
            "      \"aisle\": \"Produce\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/avocado.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"avocados\"," +
            "      \"amount\": 2," +
            "      \"unit\": \"\"," +
            "      \"unitShort\": \"\"," +
            "      \"unitLong\": \"\"," +
            "      \"originalString\": \"2 avocados, thinly sliced\"," +
            "      \"metaInformation\": [" +
            "        \"thinly sliced\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 20027," +
            "      \"aisle\": \"Baking\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/white-powder.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"cornstarch\"," +
            "      \"amount\": 2," +
            "      \"unit\": \"tablespoons\"," +
            "      \"unitShort\": \"Tbsp\"," +
            "      \"unitLong\": \"tablespoons\"," +
            "      \"originalString\": \"2 tablespoons cornstarch\"," +
            "      \"metaInformation\": []" +
            "    }," +
            "    {" +
            "      \"id\": 11215," +
            "      \"aisle\": \"Produce\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/garlic.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"garlic cloves\"," +
            "      \"amount\": 2," +
            "      \"unit\": \"\"," +
            "      \"unitShort\": \"\"," +
            "      \"unitLong\": \"\"," +
            "      \"originalString\": \"2 garlic cloves, minced\"," +
            "      \"metaInformation\": [" +
            "        \"minced\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 1049," +
            "      \"aisle\": \"Milk, Eggs, Other Dairy\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/fluid-cream.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"half and half\"," +
            "      \"amount\": 1," +
            "      \"unit\": \"cups\"," +
            "      \"unitShort\": \"cup\"," +
            "      \"unitLong\": \"cup\"," +
            "      \"originalString\": \"1 cups half and half\"," +
            "      \"metaInformation\": []" +
            "    }," +
            "    {" +
            "      \"id\": 9160," +
            "      \"aisle\": \"Produce\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/lime-juice.jpg\"," +
            "      \"consistency\": \"liquid\"," +
            "      \"name\": \"juice of lime\"," +
            "      \"amount\": 1," +
            "      \"unit\": \"\"," +
            "      \"unitShort\": \"\"," +
            "      \"unitLong\": \"\"," +
            "      \"originalString\": \"1 lime, juiced\"," +
            "      \"metaInformation\": [" +
            "        \"juiced\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 1001025," +
            "      \"aisle\": \"Cheese\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/shredded-cheese-white.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"monterey jack cheese\"," +
            "      \"amount\": 8," +
            "      \"unit\": \"ounces\"," +
            "      \"unitShort\": \"oz\"," +
            "      \"unitLong\": \"ounces\"," +
            "      \"originalString\": \"8 ounces monterey jack cheese, freshly grated\"," +
            "      \"metaInformation\": [" +
            "        \"freshly grated\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 4053," +
            "      \"aisle\": \"Oil, Vinegar, Salad Dressing\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/olive-oil.jpg\"," +
            "      \"consistency\": \"liquid\"," +
            "      \"name\": \"olive oil\"," +
            "      \"amount\": 1," +
            "      \"unit\": \"tablespoon\"," +
            "      \"unitShort\": \"Tbsp\"," +
            "      \"unitLong\": \"tablespoon\"," +
            "      \"originalString\": \"1 tablespoon olive oil\"," +
            "      \"metaInformation\": []" +
            "    }," +
            "    {" +
            "      \"id\": 1002030," +
            "      \"aisle\": \"Spices and Seasonings\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/pepper.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"pepper\"," +
            "      \"amount\": 0.125," +
            "      \"unit\": \"teaspoon\"," +
            "      \"unitShort\": \"tsp\"," +
            "      \"unitLong\": \"teaspoons\"," +
            "      \"originalString\": \"1/8 teaspoon pepper\"," +
            "      \"metaInformation\": []" +
            "    }," +
            "    {" +
            "      \"id\": 2047," +
            "      \"aisle\": \"Spices and Seasonings\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/salt.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"salt\"," +
            "      \"amount\": 0.125," +
            "      \"unit\": \"teaspoon\"," +
            "      \"unitShort\": \"tsp\"," +
            "      \"unitLong\": \"teaspoons\"," +
            "      \"originalString\": \"1/8 teaspoon salt\"," +
            "      \"metaInformation\": []" +
            "    }," +
            "    {" +
            "      \"id\": 1031009," +
            "      \"aisle\": \"Cheese\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/cheddar-cheese.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"sharp cheddar cheese\"," +
            "      \"amount\": 16," +
            "      \"unit\": \"ounces\"," +
            "      \"unitShort\": \"oz\"," +
            "      \"unitLong\": \"ounces\"," +
            "      \"originalString\": \"16 ounces sharp white cheddar cheese, freshly grated\"," +
            "      \"metaInformation\": [" +
            "        \"white\"," +
            "        \"freshly grated\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 11294," +
            "      \"aisle\": \"Produce\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/sweet-onion.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"sweet onion\"," +
            "      \"amount\": 0.5," +
            "      \"unit\": \"\"," +
            "      \"unitShort\": \"\"," +
            "      \"unitLong\": \"\"," +
            "      \"originalString\": \"1/2 sweet onion, diced\"," +
            "      \"metaInformation\": [" +
            "        \"diced\"," +
            "        \"sweet\"" +
            "      ]" +
            "    }," +
            "    {" +
            "      \"id\": 1145," +
            "      \"aisle\": \"Milk, Eggs, Other Dairy\"," +
            "      \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/butter-sliced.jpg\"," +
            "      \"consistency\": \"solid\"," +
            "      \"name\": \"unsalted butter\"," +
            "      \"amount\": 1," +
            "      \"unit\": \"tablespoon\"," +
            "      \"unitShort\": \"Tbsp\"," +
            "      \"unitLong\": \"tablespoon\"," +
            "      \"originalString\": \"1 tablespoon unsalted butter\"," +
            "      \"metaInformation\": [" +
            "        \"unsalted\"" +
            "      ]" +
            "    }" +
            "  ]," +
            "  \"id\": 766301," +
            "  \"title\": \"Queso Cheese Burgers\"," +
            "  \"readyInMinutes\": 60," +
            "  \"image\": \"https://spoonacular.com/recipeImages/766301-556x370.jpg\"," +
            "  \"imageType\": \"jpg\"," +
            "  \"cuisines\": [" +
            "    \"american\"" +
            "  ]," +
            "  \"dishTypes\": [" +
            "    \"lunch\"," +
            "    \"main course\"," +
            "    \"main dish\"," +
            "    \"dinner\"" +
            "  ]," +
            "  \"diets\": [" +
            "    \"gluten free\"," +
            "    \"lacto ovo vegetarian\"," +
            "    \"ketogenic\"" +
            "  ]," +
            "  \"occasions\": []," +
            "  \"winePairing\": {" +
            "    \"pairedWines\": [" +
            "      \"malbec\"," +
            "      \"merlot\"," +
            "      \"zinfandel\"" +
            "    ]," +
            "    \"pairingText\": \"Malbec, Merlot, and Zinfandel are great choices for Cheeseburger. Merlot will be perfectly adequate for a classic burger with standard toppings. Bolder toppings call for bolder wines, such as a malbec or peppery zinfandel. One wine you could try is Terrazas de los Andes Reserva Malbec. It has 4.2 out of 5 stars and a bottle costs about 22 dollars.\"," +
            "    \"productMatches\": [" +
            "      {" +
            "        \"id\": 438817," +
            "        \"title\": \"Terrazas de los Andes Reserva Malbec\"," +
            "        \"description\": \"Bright red color with purple shades. Intense floral and fruity notes. Presence of violets, ripe black cherry and plum aromas. ReIts sweet and juicy mouthfeel delivers finesse, delicate tannins and an elegant finish of black fruits. veals a toasty and spicy character of black pepper and chocolate.Pair with Tuna Carpaccio and Red Fruits Vinaigrette, Tenderloin served with MediterraneanRatatouille and Moist Semisweet Chocolate Cake over Mascarpone and Red Fruits Ice Cream.\"," +
            "        \"price\": \"$21.99\"," +
            "        \"imageUrl\": \"https://spoonacular.com/productImages/438817-312x231.jpg\"," +
            "        \"averageRating\": 0.8400000000000001," +
            "        \"ratingCount\": 5," +
            "        \"score\": 0.7775000000000001," +
            "        \"link\": \"https://click.linksynergy.com/deeplink?id=*QCiIS6t4gA&mid=2025&murl=https%3A%2F%2Fwww.wine.com%2Fproduct%2Fterrazas-de-los-andes-reserva-malbec-2014%2F162923\"" +
            "      }" +
            "    ]" +
            "  }," +
            "  \"instructions\": \"burgersPlace the beef in a bowl and season with the salt and pepper. Mix with your hands to incorporate, forming 4 to 6 patties, whichever size you wish and whatever size fits your bun. Heat a large skillet (or thegrill, your preference) over medium heat and add the olive oil and butter. Cook the patties until browned on both sides and your desired doneness is reached. I do about 3 to 4 minutes per side for medium-well, but it will depend on how think your burgers are.In a bowl, mix together the tomatoes, onion, cilantro and lime juiced, stirring well. Season with a pinch of salt and pepper.To assemble your burgers, cover the bottom bun with avocado slices. Place the burger on top and drizzle on lots of queso cheese. Top with the pico and the jalapeos. Serve immediately.queso cheeseHeat a saucepan over medium heat and add the olive oil and butter. Stir in the onions and garlic. Season with the salt and pepper. Cook until the onion softens, about 5 to 6 minutes. Slowly stream in 3/4 cup of the half and half, whisking the entire time. Toss the grated cheese with 1 tablespoon of the cornstarch. In a bowl, whisk together the remaining 1/4 cup half and half and 1 tablespoon cornstarch until no lumps remain to create a slurry. Stir the slurry into the saucepan and cook for a minute until the milk thickens. Reduce the heat to low.Stir in the grated cheese, one small handful at a time, until melted. Transfer the mixture to a crock, larger bowl or a small crockpot heating on low. Spoon on the burgers and serve the extra with chips!\"," +
            "  \"analyzedInstructions\": [" +
            "    {" +
            "      \"name\": \"\"," +
            "      \"steps\": [" +
            "        {" +
            "          \"number\": 1," +
            "          \"step\": \"burgers\"," +
            "          \"ingredients\": []," +
            "          \"equipment\": []" +
            "        }," +
            "        {" +
            "          \"number\": 2," +
            "          \"step\": \"Place the beef in a bowl and season with the salt and pepper.\"," +
            "          \"ingredients\": [" +
            "            {" +
            "              \"id\": 1102047," +
            "              \"name\": \"salt and pepper\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/salt-and-pepper.jpg\"" +
            "            }" +
            "          ]," +
            "          \"equipment\": [" +
            "            {" +
            "              \"id\": 404783," +
            "              \"name\": \"bowl\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/bowl.jpg\"" +
            "            }" +
            "          ]" +
            "        }," +
            "        {" +
            "          \"number\": 3," +
            "          \"step\": \"Mix with your hands to incorporate, forming 4 to 6 patties, whichever size you wish and whatever size fits your bun.\"," +
            "          \"ingredients\": []," +
            "          \"equipment\": []" +
            "        }," +
            "        {" +
            "          \"number\": 4," +
            "          \"step\": \"Heat a large skillet (or thegrill, your preference) over medium heat and add the olive oil and butter. Cook the patties until browned on both sides and your desired doneness is reached. I do about 3 to 4 minutes per side for medium-well, but it will depend on how think your burgers are.In a bowl, mix together the tomatoes, onion, cilantro and lime juiced, stirring well. Season with a pinch of salt and pepper.To assemble your burgers, cover the bottom bun with avocado slices.\"," +
            "          \"ingredients\": [" +
            "            {" +
            "              \"id\": 1102047," +
            "              \"name\": \"salt and pepper\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/salt-and-pepper.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 1019037," +
            "              \"name\": \"avocado slices\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/avocado-slices.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 4053," +
            "              \"name\": \"olive oil\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/olive-oil.jpg\"" +
            "            }" +
            "          ]," +
            "          \"equipment\": [" +
            "            {" +
            "              \"id\": 404645," +
            "              \"name\": \"frying pan\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/pan.png\"" +
            "            }," +
            "            {" +
            "              \"id\": 404783," +
            "              \"name\": \"bowl\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/bowl.jpg\"" +
            "            }" +
            "          ]," +
            "          \"length\": {" +
            "            \"number\": 3," +
            "            \"unit\": \"minutes\"" +
            "          }" +
            "        }," +
            "        {" +
            "          \"number\": 5," +
            "          \"step\": \"Place the burger on top and drizzle on lots of queso cheese. Top with the pico and the jalapeos.\"," +
            "          \"ingredients\": [" +
            "            {" +
            "              \"id\": 1041009," +
            "              \"name\": \"cheese\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/cheddar-cheese.jpg\"" +
            "            }" +
            "          ]," +
            "          \"equipment\": []" +
            "        }," +
            "        {" +
            "          \"number\": 6," +
            "          \"step\": \"Serve immediately.queso cheese\"," +
            "          \"ingredients\": [" +
            "            {" +
            "              \"id\": 1041009," +
            "              \"name\": \"cheese\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/cheddar-cheese.jpg\"" +
            "            }" +
            "          ]," +
            "          \"equipment\": []" +
            "        }," +
            "        {" +
            "          \"number\": 7," +
            "          \"step\": \"Heat a saucepan over medium heat and add the olive oil and butter. Stir in the onions and garlic. Season with the salt and pepper. Cook until the onion softens, about 5 to 6 minutes. Slowly stream in 3/4 cup of the half and half, whisking the entire time. Toss the grated cheese with 1 tablespoon of the cornstarch. In a bowl, whisk together the remaining 1/4 cup half and half and 1 tablespoon cornstarch until no lumps remain to create a slurry. Stir the slurry into the saucepan and cook for a minute until the milk thickens. Reduce the heat to low.Stir in the grated cheese, one small handful at a time, until melted.\"," +
            "          \"ingredients\": [" +
            "            {" +
            "              \"id\": 1102047," +
            "              \"name\": \"salt and pepper\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/salt-and-pepper.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 1049," +
            "              \"name\": \"half and half\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/fluid-cream.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 20027," +
            "              \"name\": \"corn starch\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/white-powder.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 4053," +
            "              \"name\": \"olive oil\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/olive-oil.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 1041009," +
            "              \"name\": \"cheese\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/cheddar-cheese.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 11215," +
            "              \"name\": \"garlic\"," +
            "              \"image\": \"https://spoonacular.com/cdn/ingredients_100x100/garlic.jpg\"" +
            "            }" +
            "          ]," +
            "          \"equipment\": [" +
            "            {" +
            "              \"id\": 404669," +
            "              \"name\": \"sauce pan\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/sauce-pan.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 404661," +
            "              \"name\": \"whisk\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/whisk.png\"" +
            "            }," +
            "            {" +
            "              \"id\": 404783," +
            "              \"name\": \"bowl\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/bowl.jpg\"" +
            "            }" +
            "          ]," +
            "          \"length\": {" +
            "            \"number\": 5," +
            "            \"unit\": \"minutes\"" +
            "          }" +
            "        }," +
            "        {" +
            "          \"number\": 8," +
            "          \"step\": \"Transfer the mixture to a crock, larger bowl or a small crockpot heating on low. Spoon on the burgers and serve the extra with chips!\"," +
            "          \"ingredients\": []," +
            "          \"equipment\": [" +
            "            {" +
            "              \"id\": 404718," +
            "              \"name\": \"slow cooker\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/slow-cooker.jpg\"" +
            "            }," +
            "            {" +
            "              \"id\": 404783," +
            "              \"name\": \"bowl\"," +
            "              \"image\": \"https://spoonacular.com/cdn/equipment_100x100/bowl.jpg\"" +
            "            }" +
            "          ]" +
            "        }" +
            "      ]" +
            "    }" +
            "  ]" +
            "}";
    public static String TEST_FIND_A_WINE_STRING = "{" +
            "  \"pairedWines\": [" +
            "    \"merlot\"," +
            "    \"cabernet sauvignon\"," +
            "    \"pinot noir\"" +
            "  ]," +
            "  \"pairingText\": \"Merlot, Cabernet Sauvignon, and Pinot Noir are my top picks for Steak. After all, beef and red wine are a classic combination. Generally, leaner steaks go well with light or medium-bodied reds, such as pinot noir or merlot, while fattier steaks can handle a bold red, such as cabernet sauvingnon. The Franciscan Estate Merlot with a 4.3 out of 5 star rating seems like a good match. It costs about 25 dollars per bottle.\"," +
            "  \"productMatches\": [" +
            "    {" +
            "      \"id\": 443286," +
            "      \"title\": \"Franciscan Estate Merlot\"," +
            "      \"description\": \"Our Oakville estate produces distinctive, supple Merlot characterized by flavors of cherries and plums. This wine shows rich, lush and velvety fruit flavors of cherries, plums and blackberries that merge with light toasted oak. Blended with a samll percentage of Cabernet Sauvignon and Cabernet Franc this wine is well balanced and structured with a lingering finish.Alcohol: 13.5%\"," +
            "      \"price\": \"$25.49\"," +
            "      \"imageUrl\": \"https://spoonacular.com/productImages/443286-312x231.jpg\"," +
            "      \"averageRating\": 0.86," +
            "      \"ratingCount\": 5," +
            "      \"score\": 0.7975," +
            "      \"link\": \"https://click.linksynergy.com/deeplink?id=*QCiIS6t4gA&mid=2025&murl=https%3A%2F%2Fwww.wine.com%2Fproduct%2Ffranciscan-estate-merlot-1998%2F7308\"" +
            "    }" +
            "  ]" +
            "}";

}
