package com.example.tiami.intsysproject;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tiami.intsysproject.constants.Constants;

import java.util.ArrayList;


/**
 * Created by tiami on 05/12/2017.
 */

public class FilterActivity extends AppCompatActivity {

    private ArrayList<String> excluded;
    private TextView tvProgressLabel;
    private TextView tvProteinLabel;
    private ArrayList<String> allergies;
    private ArrayList<String> diets;
    private int currentProtein;
    private int currentCalory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_activity);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        excluded = new ArrayList<String>();
        Log.d(Constants.TAG, "Created filter activity");

        // set a change listener on the SeekBars
        SeekBar seekBar = findViewById(R.id.seekBar);
        SeekBar proteinSeek = findViewById(R.id.protSeekBar);
        seekBar.setOnSeekBarChangeListener(seekBarChangeListener);
        proteinSeek.setOnSeekBarChangeListener(protSeekChangeListener);

        int progress = seekBar.getProgress();
        tvProgressLabel = findViewById(R.id.textView);
        tvProgressLabel.setText("Max: " + progress);
        currentCalory = progress;
        int progressProtein = proteinSeek.getProgress();
        tvProteinLabel = findViewById(R.id.protTextView);
        tvProteinLabel.setText("Min: " + progressProtein);
        currentProtein = progressProtein;
    }

    SeekBar.OnSeekBarChangeListener seekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            // updated continuously as the user slides the thumb
            tvProgressLabel.setText("Max: " + progress);
            currentCalory = progress;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {        }
    };

    SeekBar.OnSeekBarChangeListener protSeekChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            // updated continuously as the user slides the thumb
            tvProteinLabel.setText("Min: " + progress);
            currentProtein = progress;
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {        }
    };

    public void excludeIngredient(View view) {
        EditText ingredient = findViewById(R.id.exclField);
        excluded.add(ingredient.getText().toString());

        ListView lw = findViewById(R.id.listExclude);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                excluded);
        lw.setAdapter(arrayAdapter);

        ingredient.setText("");
    }

    public void allergySelect(View view) {

        final Dialog dialog = new Dialog(this); // Context, this, etc.
        dialog.setContentView(R.layout.dialog_allergy);
        dialog.setTitle("Choose Allergy");

        Button button = (Button) dialog.findViewById(R.id.dialog_cancelAllergy);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        Button okButton = (Button) dialog.findViewById(R.id.dialog_okAllergy);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                okAllergies(dialog);

                Context context = getApplicationContext();
                CharSequence text = "Allergies added";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                dialog.dismiss();

            }
        });

        dialog.show();
    }

    public void dietSelect(View view) {

            final Dialog dialog = new Dialog(this); // Context, this, etc.
            dialog.setContentView(R.layout.dialog_diet);
            dialog.setTitle("Choose Diets");

        Button cancelButton = (Button) dialog.findViewById(R.id.dialog_cancelDiet);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        Button okButton = (Button) dialog.findViewById(R.id.dialog_okDiet);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                okDiets(dialog);

                Context context = getApplicationContext();
                CharSequence text = "Diets added";
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                dialog.dismiss();

            }
        });
        dialog.show();
    }

    public void okAllergies(Dialog dialog) {
        CheckBox dairy = dialog.findViewById(R.id.dairy);
        CheckBox egg = dialog.findViewById(R.id.egg);
        CheckBox gluten = dialog.findViewById(R.id.gluten);
        CheckBox grain = dialog.findViewById(R.id.grain);
        CheckBox seafood = dialog.findViewById(R.id.seafood);
        CheckBox shellfish = dialog.findViewById(R.id.shellfish);
        CheckBox wheat = dialog.findViewById(R.id.wheat);
        CheckBox soy = dialog.findViewById(R.id.soy);
        CheckBox peanut = dialog.findViewById(R.id.peanut);
        CheckBox treenut = dialog.findViewById(R.id.treenut);

        allergies = new ArrayList<>();
        if(dairy.isChecked()){
            allergies.add("dairy");
        }
        if(egg.isChecked()){
            allergies.add("egg");
        }
        if(gluten.isChecked()){
            allergies.add("gluten");
        }
        if(grain.isChecked()){
            allergies.add("grain");
        }
        if(seafood.isChecked()){
            allergies.add("seafood");
        }
        if(shellfish.isChecked()){
            allergies.add("shellfish");
        }
        if(wheat.isChecked()){
            allergies.add("wheat");
        }
        if(soy.isChecked()){
            allergies.add("soy");
        }
        if(peanut.isChecked()){
            allergies.add("peanut");
        }
        if(treenut.isChecked()){
            allergies.add("treenut");
        }
    }

    public void okDiets(Dialog dialog) {
        CheckBox paleo = dialog.findViewById(R.id.paleo);
        CheckBox vegan = dialog.findViewById(R.id.vegan);
        CheckBox vegetarian = dialog.findViewById(R.id.vegetarian);
        CheckBox whole30 = dialog.findViewById(R.id.whole30);

        diets = new ArrayList<>();
        if(paleo.isChecked()){
            diets.add("paleo");
        }
        if(vegan.isChecked()){
            diets.add("vegan");
        }
        if(vegetarian.isChecked()){
            diets.add("vegetarian");
        }
        if(whole30.isChecked()){
            diets.add("whole30");
        }
    }

    public void addFilters(View view) {

        Intent intent=new Intent();
        StringBuilder sb = new StringBuilder();
        if(allergies!=null) {
            for (String s : allergies) {
                sb.append(s + ",");
            }
            String allergyMessage = sb.toString();
            intent.putExtra("ALLERGY",allergyMessage);
        }


        StringBuilder sb2 = new StringBuilder();
        if(diets!=null) {
            for (String s : diets) {
                sb2.append(s + ",");
            }
            String dietMessage = sb2.toString();
            intent.putExtra("DIET", dietMessage);
        }

        intent.putExtra("PROTEIN", currentProtein);
        intent.putExtra("CALORY",currentCalory);
        intent.putExtra("EXCLUDED",excluded);
        setResult(2,intent);
        finish();
    }

    public void closeFilters(View view) {
        finish();
    }
}
