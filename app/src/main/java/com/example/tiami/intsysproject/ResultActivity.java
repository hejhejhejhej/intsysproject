package com.example.tiami.intsysproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.tiami.intsysproject.adapter.RecipeListAdapter;
import com.example.tiami.intsysproject.constants.Constants;
import com.example.tiami.intsysproject.spoonacularJSONClasses.SearchRecipesModel;
import com.google.gson.Gson;
import com.rapidapi.rapidconnect.RapidApiConnect;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Thread.sleep;

public class ResultActivity extends AppCompatActivity {
    private RapidApiConnect connect;
    private Gson gson;
    private SearchRecipesModel recipes;
    private String recipeJSON;

    ListView androidListView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_activity);

        Log.d(Constants.TAG, "Created result activity");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gson = new Gson();

        //recipeJSON = Constants.RESULT_TEST_STRING;
        recipeJSON = getIntent().getExtras().getString("RECIPES");
        recipes = gson.fromJson(recipeJSON, SearchRecipesModel.class);

        androidListView = (ListView) findViewById(R.id.list);
        RecipeListAdapter recipeListAdapter = new RecipeListAdapter(getBaseContext(), recipes.getResults());
        androidListView.setAdapter(recipeListAdapter);

        androidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Log.i(Constants.TAG, "You clicked Item: " + recipes.getResults().get(position).getTitle());
                sendRequest(String.valueOf(recipes.getResults().get(position).getId()));

            }
        });
    }

    private void sendRequest(String recipeID) {
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="https://spoonacular-recipe-food-nutrition-v1.p.mashape.com/recipes/" + recipeID + "/information?includeNutrition=false";

        // Request a string response
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //save response
                        System.out.println("Response: " + response);
                        startRecipeActivity(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("That didn't work!");
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("X-Mashape-Key", "n2gURhadr4msh3sohOaqzDXJ2E6ap1wd9fljsnoZ2WltuxnMmm");
                params.put("Accept", "application/json");
                return params;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    private void startRecipeActivity(String response) {
        Intent intent = new Intent(ResultActivity.this, RecipeActivity.class);
        System.out.println("recipeAsJSON: " + response);
        intent.putExtra("recipe", response);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Decides what menu items should do
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        CharSequence title = item.getTitle();
        changeActivity(title);
        return true;
    }

    //call changeActivity according to the selected option
    private void changeActivity(CharSequence title) {
        if(title.equals("Favourites")){
            Intent intent = new Intent(ResultActivity.this, FavouriteActivity.class);
            startActivity(intent);
        } else if(title.equals("FindWine")) {
            Intent intent = new Intent(ResultActivity.this, FindWineActivity.class);
            startActivity(intent);
        } else if(title.equals("About")) {
            Intent intent = new Intent(ResultActivity.this, AboutActivity.class);
            startActivity(intent);
        } else if(title.equals("Home")) {
            Intent intent = new Intent(ResultActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }

}
