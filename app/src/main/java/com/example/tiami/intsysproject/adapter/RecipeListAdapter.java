package com.example.tiami.intsysproject.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tiami.intsysproject.R;
import com.example.tiami.intsysproject.asyncTasks.DownloadImageTask;
import com.example.tiami.intsysproject.spoonacularJSONClasses.RecipeListItem;

import java.util.ArrayList;

/**
 * Created by Soren on 04/12/2017.
 */

public class RecipeListAdapter extends BaseAdapter {
    private Activity activity;
    private Context context;
    private ArrayList<RecipeListItem> data;
    private String baseURI = "https://spoonacular.com/recipeImages/";


    public RecipeListAdapter(Context context, ArrayList<RecipeListItem> d) {
        this.context = context;
        data = d;
    }


    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder {

        public TextView text;
        public ImageView image;

    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View v = View.inflate(context, R.layout.recipe_list, null);
        TextView name = (TextView) v.findViewById(R.id.Itemname);
        ImageView image = (ImageView) v.findViewById(R.id.icon);
        System.out.println(data.get(position).getImage());
        new DownloadImageTask(image).execute(data.get(position).getImage());

        // Set text
        name.setText(data.get(position).getTitle());

        v.setTag(data.get(position).getId());
        return v;
    }


}
