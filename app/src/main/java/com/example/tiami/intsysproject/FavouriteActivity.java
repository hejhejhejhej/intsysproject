package com.example.tiami.intsysproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.tiami.intsysproject.adapter.RecipeListAdapter;
import com.example.tiami.intsysproject.constants.Constants;
import com.example.tiami.intsysproject.spoonacularJSONClasses.Recipe;
import com.example.tiami.intsysproject.spoonacularJSONClasses.RecipeListItem;
import com.example.tiami.intsysproject.spoonacularJSONClasses.SearchRecipesModel;
import com.google.gson.Gson;

import java.util.ArrayList;

public class FavouriteActivity extends AppCompatActivity {
    Gson gson;
    ListView androidListView;
    private ArrayList<RecipeListItem> recipes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.d(Constants.TAG, "Created favourite activity");

        gson = new Gson();

        SharedPreferences prefs = getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        String favouriteKey = "com.example.app.favourites";
        String fromPreferences = prefs.getString(favouriteKey,"");
        final SearchRecipesModel favourites;
        if (fromPreferences.isEmpty()) {
            favourites = new SearchRecipesModel(new ArrayList<RecipeListItem>(), Constants.BASE_URI);
        } else {
            favourites = gson.fromJson(fromPreferences, SearchRecipesModel.class);
        }
        recipes = favourites.getResults();

        if (recipes.isEmpty()) {
            TextView textView = (TextView)findViewById(R.id.favouriteMissingText);
            textView.setText(R.string.noFavour);
        }

        androidListView = (ListView) findViewById(R.id.list);
        RecipeListAdapter recipeListAdapter = new RecipeListAdapter(getBaseContext(), favourites.getResults());
        androidListView.setAdapter(recipeListAdapter);

        androidListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Log.i(Constants.TAG, "You clicked Item: " + favourites.getResults().get(position).getTitle());
                String response = gson.toJson(favourites.getResults().get(position).getRecipe(), Recipe.class);
                startRecipeActivity(response);
            }
        });

    }

    private void startRecipeActivity(String response) {
        Intent intent = new Intent(FavouriteActivity.this, RecipeActivity.class);
        System.out.println("recipeAsJSON: " + response);
        intent.putExtra("recipe", response);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //Decides what menu items should do
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        CharSequence title = item.getTitle();
        changeActivity(title);
        return true;
    }

    //call changeActivity according to the selected option
    private void changeActivity(CharSequence title) {
        if(title.equals("Favourites")){
            Intent intent = new Intent(FavouriteActivity.this, FavouriteActivity.class);
            startActivity(intent);
        } else if(title.equals("FindWine")) {
            Intent intent = new Intent(FavouriteActivity.this, FindWineActivity.class);
            startActivity(intent);
        } else if(title.equals("About")) {
            Intent intent = new Intent(FavouriteActivity.this, AboutActivity.class);
            startActivity(intent);
        } else if(title.equals("Home")) {
            Intent intent = new Intent(FavouriteActivity.this, MainActivity.class);
            startActivity(intent);
        }
    }
}
