package com.example.tiami.intsysproject;

import com.example.tiami.intsysproject.constants.Constants;
import com.example.tiami.intsysproject.spoonacularJSONClasses.RecipeListItem;
import com.example.tiami.intsysproject.spoonacularJSONClasses.SearchRecipesModel;
import com.google.gson.Gson;
import com.mashape.unirest.http.Unirest;

import org.junit.Test;

import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
        Gson gson = new Gson();
        SearchRecipesModel results = gson.fromJson(Constants.RESULT_TEST_STRING, SearchRecipesModel.class);
        assertThat(results.getBaseUri(), is("https://spoonacular.com/recipeImages/"));
        assertThat(results.getOffset(), is(0));
        assertThat(results.getNumber(), is(10));
        assertThat(results.getTotalResults(), is(10));
        assertThat(results.getProcessingTimeMs(), is(323));
        Long expires = 1473587241426L;
        assertThat(results.getExpires(), is(expires));
        assertThat(results.isStale(), is(false));

        ArrayList<RecipeListItem> items = results.getResults();
        RecipeListItem item = items.get(0);
        assertThat(item.getId(), is(262682));
        assertThat(item.getTitle(), is("Thai Sweet Potato Veggie Burgers with Spicy Peanut Sauce"));
        assertThat(item.getReadyInMinutes(), is(75));
        assertThat(item.getImage(), is("thai-sweet-potato-veggie-burgers-with-spicy-peanut-sauce-262682.jpg"));

        ClassLoader classLoader = RecipeActivity.class.getClassLoader();
        URL resource = classLoader.getResource("org/apache/http/message/BasicLineFormatter.class");
        System.out.println(resource);

        try {
            System.out.println(Unirest.get("http://www.google.com").asStringAsync().get().getBody());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}